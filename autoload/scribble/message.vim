let s:fileNameRequire = 'tile( file extension is required ) : '
let s:irregularPath = 'irregular path is : '

function! scribble#message#New()
    let l:object = {}

    " init
    let l:object.fileNameRequire = s:fileNameRequire
    let l:object.irregularPath = s:irregularPath




    function! l:object.setFileNameRequire(message) dict
        let self.fileNameRequire = a:message
    endfunction

    function! l:object.getFileNameRequire() dict
        return self.fileNameRequire
    endfunction




    function! l:object.seetIrregularPath(message) dict
        let self.irregularPath = a:message
    endfunction

    function! l:object.getIrregularPath() dict
        return self.irregularPath
    endfunction

    return l:object
endfunction
