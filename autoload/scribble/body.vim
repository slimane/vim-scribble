function! scribble#body#main(...)
    let Util            = scribble#util#New()
    let DefaultVal    = scribble#defaultVal#New()
    let Message       = scribble#message#New()

    let l:fileName = s:createFileName(a:000)
    if l:fileName ==? ''
        return
    endif

    let l:dir = s:getDirName()
    if !Util.createDir(l:dir)
        echoerr Message.getIrregularPath() . l:dir
        return
    endif


    let l:newBufCommmand =
    \   Util.varOverridesDefault('g:scribbleNewCommand'
    \                       , DefaultVal.getNewCommand())
    let l:newBufCommmand .= ' new'
    let l:newBufCommmand .= ' | edit!'

    execute l:newBufCommmand . ' ' . l:dir . '/' . l:fileName
endfunction




function! s:getDirName()
    let Util = scribble#util#New()
    let DefaultVal = scribble#defaultVal#New()

    let l:dir = Util.varOverridesDefault('g:scribblePath', DefaultVal.getDotScribble())
    return expand(Util.cutLastBackSlashOrSlash(l:dir))
endfunction




function! s:createFileName(argsList)
    let Util          = scribble#util#New()
    let Message       = scribble#message#New()
    let DefaultVal    = scribble#defaultVal#New()


    let l:argList = filter(deepcopy(a:argsList), "v:val !=? '' ")
    let l:fileName = len(l:argList) > 0
    \                   ? get(a:argsList, 0)
    \                   : input(Message.getFileNameRequire())
    let l:filename = Util.fileNameFormat(l:fileName)

    if l:fileName ==? ''
        return ''
    endif

    if exists('g:scribbleUserDate') && g:scribbleUserDate ==? 0
        return l:fileName
    endif

    let l:dateFormat =
    \       Util.varOverridesDefault('g:scribbleDateFormat'
    \                               , DefaultVal.getDateFormat())

    return strftime(l:dateFormat) . '_' . l:fileName
endfunction
