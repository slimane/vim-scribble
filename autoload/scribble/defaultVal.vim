let s:dotScribble = '~/.vim/scribble'
let s:dateFormat  = '%d%m%Y_%H%M'
let s:newCommand  = 'vertical'

function! scribble#defaultVal#New()
    let l:object = {}

    " init
    let l:object.dotScribble = s:dotScribble
    let l:object.dateFormat  = s:dateFormat
    let l:object.newCommand  = s:newCommand




    function! l:object.setDotScribble(path) dict
        let self.dotScribble = a:path
    endfunction

    function! l:object.getDotScribble() dict
        return self.dotScribble
    endfunction



    function! l:object.setDateFormat(dateFormat) dict
        let self.dateFormat = a:dateFormat
    endfunction

    function! l:object.getDateFormat() dict
        return self.dateFormat
    endfunction




    function! l:object.setNewCommand(newCommand) dict
        let self.newCommand = a:newCommand
    endfunction

    function! l:object.getNewCommand() dict
        return self.newCommand
    endfunction


    return l:object
endfunction
