function! scribble#util#New()
    let l:object = {}
    function! l:object.createDir(path)
        try
            if isdirectory(a:path)
                return 1
            endif
            call mkdir(a:path, 'p')
            return 1
        catch /E739.*/
        endtry
        return 0
    endfunction

    function! l:object.varOverridesDefault(varName, default)
        return exists(a:varName) ? eval(a:varName) : a:default
    endfunction

    function! l:object.fileNameFormat(fileName)
        return substitute(a:fileName, '\v(\\|\||''|"|\/|^)', '_', 'g')
    endfunction

    function! l:object.cutLastBackSlashOrSlash(txt)
        return substitute(a:txt, '^v(/|\\)', '', '')
    endfunction

    return l:object
endfunction
