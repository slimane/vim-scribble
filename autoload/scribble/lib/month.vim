
function! s:monthInit()
    let l:month = {}
    for l:monthNum in [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
        let l:month[l:monthNum] = {}
    endfor
    unlet l:monthNum


    let l:month[1]['en_abbr']  = 'Jan'
    let l:month[2]['en_abbr']  = 'Feb'
    let l:month[3]['en_abbr']  = 'Mar'
    let l:month[4]['en_abbr']  = 'Apr'
    let l:month[5]['en_abbr']  = 'May'
    let l:month[6]['en_abbr']  = 'Jun'
    let l:month[7]['en_abbr']  = 'Jul'
    let l:month[8]['en_abbr']  = 'Aug'
    let l:month[9]['en_abbr']  = 'Sep'
    let l:month[10]['en_abbr'] = 'Oct'
    let l:month[11]['en_abbr'] = 'Nov'
    let l:month[12]['en_abbr'] = 'Dec'

    let l:month[1]['en_full']  = 'January'
    let l:month[2]['en_full']  = 'February'
    let l:month[3]['en_full']  = 'March'
    let l:month[4]['en_full']  = 'April'
    let l:month[5]['en_full']  = 'May'
    let l:month[6]['en_full']  = 'June'
    let l:month[7]['en_full']  = 'July'
    let l:month[8]['en_full']  = 'August'
    let l:month[9]['en_full']  = 'September'
    let l:month[10]['en_full'] = 'October'
    let l:month[11]['en_full'] = 'November'
    let l:month[12]['en_full'] = 'December'

    let l:month[1]['it_abbr']   = 'Gen'
    let l:month[2]['it_abbr']   = 'Feb'
    let l:month[3]['it_abbr']   = 'Maa'
    let l:month[4]['it_abbr']   = 'Apr'
    let l:month[5]['it_abbr']   = 'Mag'
    let l:month[6]['it_abbr']   = 'Giu'
    let l:month[7]['it_abbr']   = 'Lug'
    let l:month[8]['it_abbr']   = 'Ago'
    let l:month[9]['it_abbr']   = 'Set'
    let l:month[10]['it_abbr']  = 'Ott'
    let l:month[11]['it_abbr']  = 'Nov'
    let l:month[12]['it_abbr']  = 'Dic'

    let l:month[1]['it_full']   = 'Gennaio'
    let l:month[2]['it_full']   = 'Febbraio'
    let l:month[3]['it_full']   = 'Maarzo'
    let l:month[4]['it_full']   = 'Aprile'
    let l:month[5]['it_full']   = 'Maggio'
    let l:month[6]['it_full']   = 'Giugno'
    let l:month[7]['it_full']   = 'Luglio'
    let l:month[8]['it_full']   = 'Agosto'
    let l:month[9]['it_full']   = 'Settembre'
    let l:month[10]['it_full']  = 'Ottobre'
    let l:month[11]['it_full']  = 'Novembre'
    let l:month[12]['it_full']  = 'Dicembre'

    return l:month
endfunction




function! scribble#lib#month#New()
    let l:object = {}

    " constracta
    let l:object.variable = s:monthInit()


    function! l:object.getMonthString(number, type) dict
        return self.variable[a:number + 0][a:type]
    endfunction

    " overload likeに動作せせる
    function! l:object.setMonthInfo(...) dict
        if a:0 ==?  3
        \       && a:1 =~? '^\d\+$'
        \       && type(a:2) ==? type('string')
        \       && type(a:3) ==? type('string')
            call self.setMonthInfoString(a:1, a:2, a:3)

        "elseif  todo {{{}}}形式の変数を受け取った時にsetするメソッドを作る
        else
            let l:type = enkinho#lib#util#type#New()
            let l:throwMsg =  'E117: Unknown function setMonthInfo('
            for l:val in a:000
                let l:throwMsg = l:throwMsg . l:type.typeCheck(l:val) . ', '
            endfor
            throw substitute(l:throwMsg, ',\s$', '', '') . ')'
        endif
    endfunction

    function! l:object.setMonthInfoString(num, type, val) dict
        let self.var[a:num][a:type] = a:val
    endfunction


    return l:object
endfunction
