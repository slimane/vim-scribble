let s:save_cpo = &cpo
set cpo&vim

command! -nargs=? Scribble call scribble#body#main("<args>")

let &cpo = s:save_cpo
unlet s:save_cpo
